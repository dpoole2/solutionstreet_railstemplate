# README

This is a Rails project template for Solution Street. 

We are currently running Rails 6 with the most stable version of Ruby (at this time it is 3.0.2).

Installed in this template:

* User sign up and authentication via BCrypt
* Bootstrap 4
* Datatables
* Turbolinks
* Jquery
* SQLite DB
* Basic UserMailer

