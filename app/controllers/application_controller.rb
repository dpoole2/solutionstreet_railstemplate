class ApplicationController < ActionController::Base

    private

    def current_user
        @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id]
    end
  
    def authorize
      redirect_to log_in_path unless current_user
    end
end
