class PasswordResetsController < ApplicationController
    before_action :valid_token,   only: [:edit, :update]

    def new
    end
  
    def create
      @user = User.find_by(email: params[:password_reset][:email].downcase)
      if @user
        @user.send_reset_token
        flash[:info] = "Email sent with password reset instructions"
        redirect_to root_url
      else
        flash.now[:danger] = "Email address not found"
        render 'new'
      end
    end
  
    def edit
    end
  
    def update
      if params[:user][:password].empty?                  
        @user.errors.add(:password, "can't be empty")
        render 'edit'
      elsif @user.update(user_params)
        session[:user_id] = @user.id
        flash[:success] = "Password has been reset."
        redirect_to root_path
      else
        render 'edit'
      end
    end
  
      private
  
        def user_params
          params.require(:user).permit(:email, :password, :password_confirmation)
        end	
  
      def valid_token
      @user = User.find_by(reset_token: params[:id])
      if !@user 
        flash.now[:danger] = "Invalid token"
        redirect_to root_url
      elsif @user.password_reset_expired? 
        flash.now[:danger] = "Expired token"
        redirect_to root_url
      end
    end
end
