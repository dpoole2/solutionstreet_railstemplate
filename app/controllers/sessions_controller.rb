class SessionsController < ApplicationController

    def new
    end
  
    def create
        @user = User.find_by(email: params[:email])
        if @user && @user.authenticate(params[:password])
            @user.successful_login
            session[:user_id] = @user.id 
            redirect_to root_path, :notice => "Logged in!"
        else
            if @user
                @user.failed_login
            end
                flash.now.alert = "Invalid email or password"
            render "new"
        end
    end
  
    def destroy 
      session[:user_id] = nil
        redirect_to root_url, :notice => "Logged out!"
    end
end
