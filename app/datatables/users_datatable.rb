class UsersDatatable < Datatable
	
	delegate :edit_user_path, :user_path, to: :@view

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: User.count,
      iTotalDisplayRecords: users.total_entries,
      aaData: data,
    }
  end

  private

  def data
    users.map do |user|
        [
          user.email,
          user.created_at,
          link_to("Edit", edit_user_path(user))
        ]
    end
  end

  def columns
    @columns ||= %w[ email ]
  end

  def users
    @users ||= fetch_users
  end

  def fetch_users
    users = User.all.order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    if params[:sSearch].present?
      users = users.where("LOWER(email) like :search", search: "%#{params[:sSearch].downcase}%")
    end
    users
  end
end
