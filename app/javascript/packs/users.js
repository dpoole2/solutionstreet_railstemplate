
var getUsersHTML;
var usersTable;
getUsersHTML = function() {
  usersTable = $('#users').DataTable({
    bProcessing: true,
    bServerSide: true,
    sScrollX: false,
    columns: [
        {
          "width": "6%"
        }, {
          "width": "3%",
          "bSortable": false
        }, {
          "width": "3%",
          "bSortable": false
        }
      ],
    sAjaxSource: $('#users').data('source')
  });
};

var usersSelectAll;
usersSelectAll = function() {
  $("#select-all").on("click", function() {
      if ($(this).is(":checked")) {
        $(".usr-chkbx").each(function() {
          $(this).prop("checked", true);
        })
      } else {
        $(".usr-chkbx").each(function() {
          $(this).prop("checked", false);
        })
      }   
  })
}

$(document).on('turbolinks:load', getUsersHTML);
$(document).on('turbolinks:load', usersSelectAll);
$(document).on("turbolinks:before-cache", function() {
  if ($('#users_wrapper').length === 1) {
    return usersTable.destroy();
  }
});  





