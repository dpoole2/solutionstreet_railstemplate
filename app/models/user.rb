class User < ApplicationRecord
    has_secure_password
    before_save { self.email = email.downcase }	

    def failed_login
      self.update(failed_login_attempts: self.failed_login_attempts + 1)
    end

    def successful_login
      self.update(succesful_login_attempts: self.succesful_login_attempts + 1)
    end

    def send_reset_token
        self.reset_token = User.new_token
        update_attribute(:reset_sent_at, Time.zone.now)
        self.save!
        UserMailer.password_reset(self).deliver_now
      end
    
      def password_reset_expired?
        reset_sent_at < 2.hours.ago
      end
      
      def User.new_token
        SecureRandom.urlsafe_base64
      end


      private
    
        # Converts email to all lower-case.
        def downcase_email
          self.email = email.downcase
        end
end
