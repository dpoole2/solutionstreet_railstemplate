class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.string :reset_hash
      t.datetime :reset_sent_at
      t.string :reset_token
      t.integer :failed_login_attempts, default: 0
      t.integer :succesful_login_attempts, default: 0
      t.timestamps
    end
  end
end
